<?php

namespace App\Controller;

use App\Entity\Exemplaire;
use App\Entity\Livre;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class ExemplesModeleController extends AbstractController
{

    // update, insert et delete, pas besoin de get Repo
    // mais persist + flush
    // persist lie objet vc bdd

    /**
     * @Route("/exemples/modele/find/one/by", name="exemples_modele")
     */
    public function exempleFindOneBy()
    {
        // création d'un Entity Manager
        // permettant d'obtenir un repository sans l'instancier
        $em = $this->getDoctrine()->getManager();

        // spécifier le repo avce lequel travailler
        // $rep = $em->getRepository("App\Entity\Livre");
        $rep = $em->getRepository(Livre::class);

        //ancienne syntaxe d'array
        // $rep->findOneBy(array('titre' => 'Odyssée'));
        $livre = $rep->findOneBy(['titre' => 'Odyssée']);

        // find by on retourne un objet VS find by ou findAll qui renvoie un tableau

        // dump & die en une seule ligne
        // dd($rep);

        dd($livre);

        return $this->render('exemples_modele/exemple_find_one_by.html.twig', [
            'livre' => $livre
        ]);
    }


     /**
     * @Route("/exemples/modele/find/by")
     */
    public function exempleFindBy()
    {
        $em = $this->getDoctrine()->getManager();
        $rep = $em->getRepository(Livre::class);

        $livres = $rep->findBy(['prix'=>30]);

        return $this->render('exemples_modele/exemple_find_by.html.twig', [
            'livres' => $livres
        ]);
    }

    /**
     * @Route("/exemples/modele/find/all")
     */
    public function exempleFindAll()
    {
        $em = $this->getDoctrine()->getManager();
        $rep = $em->getRepository(Livre::class);

        $livres = $rep->findAll();

        // dd($livres);

        return $this->render('exemples_modele/exemple_find_all.html.twig', [
            'livres' => $livres
        ]);
    }

    /**
     * @Route("/exemples/modele/find")
     */
    public function exempleFind()
    {
        $em = $this->getDoctrine()->getManager();
        $rep = $em->getRepository(Livre::class);

        $livre = $rep->find(3);

        return $this->render('exemples_modele/exemple_find.html.twig', [
            'livre' => $livre
        ]);
    }

    /**
     * @Route("/exemples/modele/insert/livre")
     */
    public function exempleInsertLivre()
    {
        $em = $this->getDoctrine()->getManager();
        $l1 = new Livre();
        $l1->setTitre("Test")->setPrix(12);

        $em->persist($l1);
        $em->flush();

        return new Response("Le livre a été inséré.");
    }


    /**
     * @Route("/exemples/modele/update/livre")
     */
    public  function exempleUpdateLivre()
    {
        $em = $this->getDoctrine()->getManager();
        $rep = $em->getRepository(Livre::class);
        
        // livre 1
        $l1 = $rep->find(2);
        $l1->setTitre("Nouveau Titre");

        $l2 = $rep->find(3);
        $l2->setTitre("Je ne sais pas");

        $em->flush();

        return new Response("Updaté");
    }

    /**
     * @Route("/exemples/modele/delete/livre")
     */
    public function exempleDeleteLivre()
    {
        $em = $this->getDoctrine()->getManager();
        $rep = $em->getRepository(Livre::class);

        $l1 = $rep->find(5);

        $em->remove($l1);

        $em->flush();

        return new Response("A été éfafacé ;-) stop!");
    }


     /**
     * @Route("/exemples/modele/rajouter/livre/exemplaires")
     */
    public function rajouterLivreExemplaires()
    {
        $em = $this->getDoctrine()->getManager();
        $rep = $em->getRepository(Livre::class);

        $l1 = new Livre();
        $l1->setTitre("Test2")->setPrix(12);

        $e1 = new Exemplaire();
        $e1->setEtat(true);
        $e1->setLivre($l1); // crée le lien àpd exemplaire vers livre
        $l1->addExemplaire($e1); // 

        $e2 = new Exemplaire();
        $e2->setEtat(false);

        return null;
    }
    
}
